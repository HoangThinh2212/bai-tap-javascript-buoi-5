/*
Input: Điểm 3 môn, điểm chuẩn, Khu vực, Đối tượng

Step
Step 1. Nhập điểm và chọn khu vực, đối tượng
Step 2. Thực hiện tính điểm sau khi nhận được dữ liệu từ điểm 3 môn, khu vực và đối tượng
Step 3. So sánh điểm vừa tính được và điểm Chuẩn đã nhập để biết được kết quả 
Step 4. Xuất ra màn hình kết quả Đậu hay Thất bại và Tổng điểm

Output: Kết quả (Đậu hay Thất bại) và tổng điểm có được
*/
function ketQua() {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var mon1 = document.getElementById("txt-diem-t1").value * 1;
  var mon2 = document.getElementById("txt-diem-t2").value * 1;
  var mon3 = document.getElementById("txt-diem-t3").value * 1;
  var diemKhuVuc = document.getElementById("txt-khu-vuc").value * 1;
  var diemDoiTuong = document.getElementById("txt-doi-tuong").value * 1;

  //   Tính điểm
  var diemSoSanh;
  diemSoSanh = mon1 + mon2 + mon3 + diemKhuVuc + diemDoiTuong;

  if (mon1 <= 0 || mon2 <= 0 || mon3 <= 0) {
    document.getElementById("result").innerHTML =
      "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
  } else {
    if (diemChuan <= diemSoSanh) {
      document.getElementById("result").innerHTML =
        "Bạn đã đậu. Tổng điểm: " + diemSoSanh;
    } else {
      document.getElementById("result").innerHTML =
        "Bạn đã rớt. Tổng điểm: " + diemSoSanh;
    }
  }
}
