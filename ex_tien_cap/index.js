// Ẩn hiện dữ liệu
function showKetNoi() {
  var loaiKh = document.getElementById("txt-loai-khach").value * 1;
  console.log("loaiKh: ", loaiKh);
  var kN = document.getElementById("so-ket-noi");
  if (loaiKh == "0" || loaiKh == "1") {
    kN.style.visibility = "hidden";
  } else if (loaiKh == "2") {
    kN.style.visibility = "visible";
  }
}
// Định dạng Tiền sang USD
function dinhDangTien(x) {
  var x = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(x);
  return x;
}

// Tính tiền Cáp
function tinhTien() {
  var loaiKh = document.getElementById("txt-loai-khach").value;
  var maKh = document.getElementById("ma-Kh").value;
  var soKenhCaoCap = document.getElementById("kenh-cao-cap").value * 1;
  var soKetNoi = document.getElementById("so-ket-noi").value * 1;
  var tienCap = 0;

  if (loaiKh == "0") {
    // Khi người dùng chưa chọn
    alert("Hãy chọn loại khách hàng");
    tienCap = dinhDangTien(tienCap);
    document.getElementById(
      "result"
    ).innerHTML = `Mã khách hàng: ${maKh} ; Tiền cáp: ${tienCap} `;
  } else if (loaiKh == "1") {
    // Tính cho DÂN
    const PHI_HOA_DON_DAN = 4.5;
    const PHI_DICH_VU = 20.5;
    const PHI_KENH_CAO_CAP = 7.5;

    //Tính
    tienCap = PHI_HOA_DON_DAN + PHI_DICH_VU + PHI_KENH_CAO_CAP * soKenhCaoCap;
    tienCap = dinhDangTien(tienCap);

    //Xuất màn hình
    document.getElementById(
      "result"
    ).innerHTML = `Mã khách hàng: ${maKh} ; Tiền cáp: ${tienCap} `;
  } else {
    // Tính cho DOANH NGHIỆP
    const PHI_HOA_DON_DOANH_NGHIEP = 15;
    const PHI_DICH_VU = 75;
    const PHI_KENH_CAO_CAP = 50;

    if (soKetNoi <= 10) {
      // Dưới 10 kết nối
      tienCap =
        PHI_HOA_DON_DOANH_NGHIEP +
        PHI_DICH_VU +
        PHI_KENH_CAO_CAP * soKenhCaoCap;
      tienCap = dinhDangTien(tienCap);

      //Xuất màn hình
      document.getElementById(
        "result"
      ).innerHTML = `Mã khách hàng: ${maKh} ; Tiền cáp: ${tienCap} `;
    } else {
      // Số kết nối > 10
      // Tạo biến chứa số kết nối thêm khi số kết nối > 10
      var soKetNoiThem = soKetNoi - 10;

      tienCap =
        PHI_HOA_DON_DOANH_NGHIEP +
        PHI_DICH_VU +
        soKetNoiThem * 5 +
        PHI_KENH_CAO_CAP * soKenhCaoCap;
      tienCap = dinhDangTien(tienCap);
      document.getElementById(
        "result"
      ).innerHTML = `Mã khách hàng: ${maKh} ; Tiền cáp: ${tienCap} `;
    }
  }
}
