function tinhTienDien() {
  var soKw = document.getElementById("txt-so-kw").value * 1;
  var hoTen = document.getElementById("txt-ho-ten").value;
  var giaTienDien = 0;

  if (soKw <= 0) {
    alert("Số kw không hợp lệ! Vui lòng nhập lại");
    document.getElementById(
      "gia-tien-dien"
    ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${giaTienDien}`;
  } else if (soKw <= 50) {
    //50km đầu tiên
    giaTienDien = soKw * 500;
    document.getElementById(
      "gia-tien-dien"
    ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${Intl.NumberFormat(
      "vn-VN"
    ).format(giaTienDien)}`;
  } else if (soKw <= 100) {
    //50km kế tiếp
    giaTienDien = 50 * 500 + (soKw - 50) * 650;
    document.getElementById(
      "gia-tien-dien"
    ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${Intl.NumberFormat(
      "vn-VN"
    ).format(giaTienDien)}`;
  } else if (soKw <= 200) {
    //100km kế tiếp
    giaTienDien = 50* 500 + 50 * 650 + (soKw - 100) * 850;
    document.getElementById(
      "gia-tien-dien"
    ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${Intl.NumberFormat(
      "vn-VN"
    ).format(giaTienDien)}`;
  } else if (soKw <= 350) {
    //150km kế tiếp
    giaTienDien = 50* 500 + 50 * 650 +  100 * 850 + (soKw-200) * 1100;
    document.getElementById(
      "gia-tien-dien"
    ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${Intl.NumberFormat(
      "vn-VN"
    ).format(giaTienDien)}`;
  } else {
    giaTienDien = 50* 500 + 50 * 650 +  100 * 850 + 150*1100 + (soKw-350) * 1300;
    document.getElementById(
      "gia-tien-dien"
    ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${Intl.NumberFormat(
      "vn-VN"
    ).format(giaTienDien)}`;
  }
}
